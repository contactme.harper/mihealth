

function fourdigits(number) {
	return (number < 1000) ? number + 1900 : number;
}
/**
 *  Update the clock on top right corner
 */
function updateTime() {
    var now = new Date();
    var h = now.getHours();
    var h = ("0" + h).slice(-2);
    var m = now.getMinutes();
    var m = ("0" + m).slice(-2);
    $("#time").html(now.toLocaleTimeString([], { hour: '2-digit', minute: "2-digit"}));

    var days = new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
    var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
    var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();

    var today =  days[now.getDay()] + ", " +
            months[now.getMonth()] + " " +
            date + ", " +
            (fourdigits(now.getYear())) ;

     $("#date").html(today);
}

/**
 *  DateTime page: update the content clock
 */
function updateDateTimeString(){
    var now = new Date();
    var h = now.getHours();
    var h = ("0" + h).slice(-2);
    var m = now.getMinutes();
    var m = ("0" + m).slice(-2);
    var time = now.toLocaleTimeString([], { hour: '2-digit', minute: "2-digit"});

    var days = new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
    var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
    var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();

    var today =  days[now.getDay()] + ", " +
            months[now.getMonth()] + " " +
            date + ", " +
            (fourdigits(now.getYear())) ;

     $("#content").html( today + '<br/>' +  time);
}




